PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsJO/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsJO/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?FIPSCode,?region,?arlabel,?arlatlabel1,?ISOCode,?enlabel,?araltlabel,?delabel,?hylabel,?helabel] IN horizontal 
    
  LET ?regionResource = resource(concat("JO_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("JO_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("JO_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "ar-Latn")
  LET ?enLanglabel = setLangTag(?enlabel, "en")
  LET ?deLanglabel = setLangTag(?delabel, "de")
  LET ?hyLanglabel = setLangTag(?hylabel, "hy")
  LET ?heLanglabel = setLangTag(?helabel, "he")
  LET ?arLanglabel = setLangTag(?arlabel, "ar")
  LET ?araltLanglabel = setLangTag(?araltlabel, "ar")
  LET ?arlat1Langlabel = setLangTag(?arlatlabel1, "ar-Latn")

CONSTRUCT {
  
  emergelModules:JOGovernorateList a emergelModules:Country;
                              rdfs:label "Governorates of Jordan" ;
                              skos:prefLabel "Governorates of Jordan" ;
                              rdfs:label "Governorates of Jordan"@en ;
                              skos:prefLabel "Governorates of Jordan"@en ;
                              rdfs:label "Gouvernements Jordaniens"@de ;
                              skos:prefLabel "Gouvernements Jordaniens"@de ;
                              rdfs:label "محافظات الأردن"@ar ;
                              skos:prefLabel "محافظات الأردن"@ar ;
                              rdfs:label "`mxhafz'at ala'rdn`"@ar-Latn ;
                              skos:prefLabel "`mxhafz'at ala'rdn`"@ar-Latn ;
                              rdfs:label "Հորդանանի վարչական բժանում"@hy ;
                              skos:prefLabel "Հորդանանի վարչական բժանում"@hy ;
                              rdfs:label "מחוזות ירדן"@he ;
                              skos:prefLabel "מחוזות ירדן"@he .
  
  emergelModules:JOGovernorateISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the districts of Jordan" ;
                                   skos:prefLabel "ISO codes for the districts of Jordan" ;
                                   rdfs:label "ISO codes for the districts of Jordan"@en ;
                                   skos:prefLabel "ISO codes for the districts of Jordan"@en ;
                                   rdfs:label "ISO 3166-2:JO - محافظات الأردن"@ar ;
                                   skos:prefLabel "ISO 3166-2:JO - محافظات الأردن"@ar ;
                                   rdfs:label "ISO 3166-2:JO - מחוזות ירדן"@he ;
                                   skos:prefLabel "ISO 3166-2:JO - מחוזות ירדן"@he ;
                                   rdfs:label "ISO 3166-2:JO - Հորդանանի վարչական բժանում"@hy ;
                                   skos:prefLabel "ISO 3166-2:JO - Հորդանանի վարչական բժանում"@hy ;
                                   rdfs:label "ISO 3166-2:JO - `mxhafz'at ala'rdn`"@ar-Latn ;
                                   skos:prefLabel "ISO 3166-2:JO - `mxhafz'at ala'rdn`"@ar-Latn .

  emergelModules:JOGovernorateFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the districts of Jordan" ;
                                   skos:prefLabel "FIPS codes for the districts of Jordan" ;
                                   rdfs:label "FIPS codes for the districts of Jordan"@en ;
                                   skos:prefLabel "FIPS codes for the districts of Jordan"@en ;
                                   rdfs:label "FIPS - محافظات الأردن"@ar ;
                                   skos:prefLabel "FIPS - محافظات الأردن"@ar ;
                                   rdfs:label "FIPS - מחוזות ירדן"@he ;
                                   skos:prefLabel "FIPS - מחוזות ירדן"@he ;
                                   rdfs:label "FIPS - Հորդանանի վարչական բժանում"@hy ;
                                   skos:prefLabel "FIPS - Հորդանանի վարչական բժանում"@hy ;
                                   rdfs:label "FIPS - `mxhafz'at ala'rdn`"@ar-Latn ;
                                   skos:prefLabel "FIPS - `mxhafz'at ala'rdn`"@ar-Latn .

}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?arLanglabel ;
                  skos:prefLabel ?deLanglabel ;
                  skos:prefLabel ?enLanglabel ;
                  skos:prefLabel ?hyLanglabel ;
                  skos:prefLabel ?heLanglabel ;
                  skos:altLabel ?arlat1Langlabel ;
                  skos:altLabel ?araltLanglabel ;
                  skos:inScheme emergelModules:JOGovernorateList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:JOGovernorateISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:JOGovernorateFIPSCodeList;
                  emergel:codeOf ?regionResource .
}