PREFIX project: <http://idi.fundacionctic.org/tabels/project/MZregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/MZregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?ISOCode,?noteen,?notept,?FIPSCode,?en,?sw] IN horizontal 
    
  LET ?regionResource = resource(concat("MZ_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("MZ_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("MZ_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
    LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?ptLabel = setLangTag(?region, "pt")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?swLabel = setLangTag(?sw, "sw")
  LET ?enNoteLabel = setLangTag(?noteen, "en")
  LET ?ptNoteLabel = setLangTag(?notept, "pt")
    

CONSTRUCT {
  
  emergelModules:MZProvinceList a emergelModules:Country;
                              rdfs:label "Provinces of Mozambique" ;
                              skos:prefLabel "Provinces of Mozambique" ;
                              rdfs:label "Provinces of Mozambique"@en ;
                              skos:prefLabel "Provinces of Mozambique"@en ;
                              rdfs:label "Províncias de Moçambique"@pt ;
                              skos:prefLabel "Províncias de Moçambique"@pt ;
                              rdfs:label "Mikoa ya Msumbiji"@sw ;
                              skos:prefLabel "Mikoa ya Msumbiji"@sw .
  
  emergelModules:MZProvinceISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the provinces of Mozambique" ;
                                   skos:prefLabel "ISO codes for the provinces of Mozambique" ;
                                   rdfs:label "ISO codes for the provinces of Mozambique"@en ;
                                   skos:prefLabel "ISO codes for the provinces of Mozambique"@en ;
                                   rdfs:label "Códigos ISO das províncias de Moçambique"@pt ;
                                   skos:prefLabel "Códigos ISO das províncias de Moçambique"@pt .

  emergelModules:MZProvinceFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the provinces of Mozambique" ;
                                   skos:prefLabel "FIPS codes for the provinces of Mozambique" ;
                                   rdfs:label "FIPS codes for the provinces of Mozambique"@en ;
                                   skos:prefLabel "FIPS codes for the provinces of Mozambique"@en ;
                                   rdfs:label "Códigos FIPS das províncias de Moçambique"@pt ;
                                   skos:prefLabel "Códigos FIPS das províncias de Moçambique"@pt .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?ptLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?swLabel ;
                  skos:note ?ptNoteLabel ;
                  skos:note ?enNoteLabel ;
                  skos:inScheme emergelModules:MZProvinceList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:MZProvinceISOCodeList;
                  emergel:codeOf ?regionResource .
}
