PREFIX project: <http://idi.fundacionctic.org/tabels/project/MXStates/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/MXStates/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?altLabel,?mmcLabel,?nahlabel,?nahAltLabel,?otoLabel,?puaLabel,?tzhLabel,?tzoLabel,?yuaLabel,?zapLabel,?note,?noteEn,?noteNah,?FIPSCountry,?FIPS,?en,?enalt] IN horizontal 
    
  LET ?regionResource = resource(concat("MX_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("MX_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?FIPSCodeResource = resource(concat("MX_DIVISION_LEVEL_1_FIPS_CODE_",?FIPS),emergelModules)
  //LET ?FIPSCountryResource = resource(concat("FIPS_COUNTRY_CODE_",?FIPSCountry),emergelModules)
  //LET ?TLDCodeResource = resource(concat("TLD_COUNTRY_CODE_",?FIPS),emergelModules)
  LET ?ISOResource = resource("MEX",euCountry)
      /*name of the country in local languages*/
      LET ?nahCountryLabel = setLangTag("Mēxihco", "nah")
      LET ?nahAltCountryLabel = setLangTag("Mēxihcatl Tlacetilīlli Tlahtohcāyōtl", "nah")
      LET ?nvCountryLabel = setLangTag("Méhigo", "nv")
      LET ?nvAltCountryLabel = setLangTag("Naakaii Bikéyah", "nv")

  LET ?esLabel = setLangTag(?region, "es")
  LET ?enLabel = setLangTag(?en, "en")
  LET ?altEsLabel = setLangTag(?altLabel, "es")
  LET ?altEnLabel = setLangTag(?enalt, "en")
  LET ?nahLabel = setLangTag(?nahlabel, "nah")
  LET ?altNahLabel = setLangTag(?nahAltLabel, "nah")
  LET ?NoteLabel = setLangTag(?note,"es")
  LET ?NoteEnLabel = setLangTag(?noteEn,"en")
  LET ?NoteNahLabel = setLangTag(?noteNah,"nah")

{ WHEN not matches(?mmcLabel,"") DO
    LET ?mmcLangLabel = setLangTag(?mmcLabel, "mmc")
  ;
  WHEN not matches(?otoLabel,"") DO
    LET ?otoLangLabel = setLangTag(?otoLabel, "oto")
  ;
  WHEN not matches(?puaLabel,"") DO
    LET ?puaLangLabel = setLangTag(?puaLabel, "pua")
  ;
  WHEN not matches(?tzhLabel,"") DO
    LET ?tzhLangLabel = setLangTag(?tzhLabel, "tzh")
  ;
  WHEN not matches(?tzoLabel,"") DO
    LET ?tzoLangLabel = setLangTag(?tzoLabel, "tzo")
  ;
  WHEN not matches(?yuaLabel,"") DO
    LET ?yuaLangLabel = setLangTag(?yuaLabel, "yua")
  ;
  WHEN not matches(?zapLabel,"") DO
    LET ?zapLangLabel = setLangTag(?zapLabel, "zap")
  
}

CONSTRUCT {
  
  emergelModules:MXStatesList a emergelModules:Country;
                              rdfs:label "States of Mexico"@en ;
                              skos:prefLabel "States of Mexico"@en ;
                              rdfs:label "States of Mexico" ;
                              skos:prefLabel "States of Mexico" ;
                              rdfs:label "Estados de México"@es ;
                              skos:prefLabel "Estados de México"@es ;                              
                              rdfs:label "Mēxihco ītlahtohcāyōhuān"@nah ;
                              skos:prefLabel "Mēxihco ītlahtohcāyōhuān"@nah .
  
  emergelModules:MXStatesISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the states of Mexico"@en ;
                                   skos:prefLabel "ISO codes for the states of Mexico"@en ;
                                   rdfs:label "ISO codes for the states of Mexico" ;
                                   skos:prefLabel "ISO codes for the states of Mexico" ;
                                   rdfs:label "Códigos ISO de los estados de México"@es ;
                                   skos:prefLabel "Códigos ISO de los estados de México"@es .
  
  emergelModules:MXStatesFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the states of Mexico"@en ;
                                   skos:prefLabel "FIPS codes for the states of Mexico"@en ;
                                   rdfs:label "FIPS codes for the states of Mexico" ;
                                   skos:prefLabel "FIPS codes for the states of Mexico" ;
                                   rdfs:label "Códigos FIPS de los estados de México"@es ;
                                   skos:prefLabel "Códigos FIPS de los estados de México"@es .
  
  //emergelModules:FIPSCountryCodeList a emergelModules:Country;
                                   //rdfs:label "FIPS country codes"@en ;
                                   //skos:prefLabel "FIPS country codes"@en .
  
  //emergelModules:TLDCountryCodeList a emergelModules:Country;
                                   //rdfs:label "Internet TLD (top-level domains) country codes"@en ;
                                   //skos:prefLabel "Internet TLD (top-level domains) country codes"@en .
}

//CONSTRUCT {
  //?ISOResource emergel:hasCode ?FIPSCountryResource .
               //emergel:hasCode ?TLDCodeResource .
  
  //?TLDCodeResource a skos:Concept ;
                   //rdfs:label ?TLD ;
                   //skos:prefLabel ?TLD ;
                   //skos:inScheme emergelModules:TLDCountryCodeList ;
                   //emergel:codeOf ?ISOResource .
  
  //?FIPSCountryResource a skos:Concept ;
                   //rdfs:label ?FIPSCountry ;
                   //skos:prefLabel ?FIPSCountry ;
                   //skos:inScheme emergelModules:FIPSCountryCodeList ;
                   //emergel:codeOf ?ISOResource .
//}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?esLabel ;
                  skos:altLabel ?altEsLabel ;
                  skos:altLabel ?altEnLabel ;
                  skos:prefLabel ?nahLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:altLabel ?altNahLabel ;
                  skos:note ?NoteLabel ;
                  skos:note ?NoteEnLabel ;
                  skos:note ?NoteNahLabel ;
                  skos:inScheme emergelModules:MXStatesList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?FIPSCodeResource ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource ;
               skos:altLabel ?nahAltCountryLabel ;
               skos:prefLabel ?nahCountryLabel ;
               skos:altLabel ?nvAltCountryLabel ;
               skos:prefLabel ?nvCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:MXStatesISOCodeList;
                  emergel:codeOf ?regionResource .
  
  ?FIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPS ;
                  skos:prefLabel ?FIPS ; 
                  skos:inScheme emergelModules:MXStatesFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT{
  ?regionResource skos:prefLabel ?mmcLangLabel .
}
CONSTRUCT{
  ?regionResource skos:prefLabel ?otoLangLabel .
}
CONSTRUCT{
  ?regionResource skos:prefLabel ?puaLangLabel .
}
CONSTRUCT{
  ?regionResource skos:prefLabel ?tzhLangLabel .
}
CONSTRUCT{
  ?regionResource skos:prefLabel ?tzoLangLabel .
}
CONSTRUCT{
  ?regionResource skos:prefLabel ?yuaLangLabel .
}
CONSTRUCT{
  ?regionResource skos:prefLabel ?zapLangLabel .
}