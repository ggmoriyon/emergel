PREFIX project: <http://idi.fundacionctic.org/tabels/project/SIregions/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/SIregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?bslabel,?hrlabel,?mklabel,?shlabel,?srlabel,?enlabel,?itlabel,?delabel,?hulabel,?FIPSCode,?ISOCode,?regionSupCode,?slalt,?SURSCode] IN horizontal 
    
  LET ?regionResource = resource(concat("SI_DIVISION_LEVEL_2_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("SI_DIVISION_LEVEL_2_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("SI_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?regionSURSCodeResource = resource(concat("SI_DIVISION_LEVEL_1_SURS_CODE_",replace(?SURSCode,"-","_")),emergelModules)
  LET ?regionSupResource = resource(concat("SI_DIVISION_LEVEL_1_",replace(?regionSupCode,"-","_")),emergelModules)

  LET ?ISOPublisher = resource("STANDARDIZATION_ORGANIZATION_ISO",emergelModules)
  LET ?FIPSPublisher = resource("STANDARDIZATION_ORGANIZATION_FIPS",emergelModules)
  LET ?SURSPublisher = resource("STANDARDIZATION_ORGANIZATION_SURS",emergelModules)

  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?slLabel = setLangTag(?region, "sl")
  LET ?bsLabel = setLangTag(?bslabel, "bs")
  LET ?hrLabel = setLangTag(?hrlabel, "hr")
  LET ?mkLabel = setLangTag(?mklabel, "mk")
  LET ?itLabel = setLangTag(?itlabel, "it")
  LET ?deLabel = setLangTag(?delabel, "de")
  LET ?huLabel = setLangTag(?hulabel, "hu")
  LET ?shLabel = setLangTag(?shlabel, "sh")
  LET ?srLabel = setLangTag(?srlabel, "sr")
  LET ?enLabel = setLangTag(?enlabel, "en")
  LET ?slaltLabel = setLangTag(?slalt, "sl")
    

CONSTRUCT {
  
  emergelModules:SIMunicipalitiesList a emergelModules:Country;
                              rdfs:label "Municipalities of Slovenia" ;
                              skos:prefLabel "Municipalities of Slovenia" ;
                              rdfs:label "Municipalities of Slovenia"@en ;
                              skos:prefLabel "Municipalities of Slovenia"@en ;
                              rdfs:label "Upravna podjela Slovenije"@bs ;
                              skos:prefLabel "Upravna podjela Slovenije"@bs ;
                              rdfs:label "Slovenski mestna občina"@hr ;
                              skos:prefLabel "Slovenski mestna občina"@hr ;
                              rdfs:label "Административна поделба на Словенија"@mk ;
                              skos:prefLabel "Административна поделба на Словенија"@mk ;
                              rdfs:label "Upravna podjela Slovenije"@sh ;
                              skos:prefLabel "Upravna podjela Slovenije"@sh ;
                              rdfs:label "Општине Словеније"@sr ;
                              skos:prefLabel "Општине Словеније"@sr ;
                              rdfs:label "Upravna delitev Slovenije: Občine v Sloveniji"@sl ;
                              skos:prefLabel "Upravna delitev Slovenije: Občine v Sloveniji"@sl ;
                              rdfs:label "Comuni della Slovenia"@it ;
                              skos:prefLabel "Comuni della Slovenia"@it ;
                              rdfs:label "Gemeinden in Slowenien"@de ;
                              skos:prefLabel "Gemeinden in Slowenien"@de ;
                              rdfs:label "Szlovénia: Községek"@hu ;
                              skos:prefLabel "Szlovénia: Községek"@hu .
  
  emergelModules:SIMunicipalitiesISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the municipalities of Slovenia" ;
                                   skos:prefLabel "ISO codes for the municipalities of Slovenia" ;
                                   rdfs:label "ISO codes for the municipalities of Slovenia"@en;
                                   skos:prefLabel "ISO codes for the municipalities of Slovenia"@en ;
                                   rdfs:label "Codici ISO 3166-2 per la Slovenia"@it ;
                                   skos:prefLabel "Codici ISO 3166-2 per la Slovenia"@it ;
                                   rdfs:label "Kode ISO 3166-2 na Slovenijo"@sl ;
                                   skos:prefLabel "Kode ISO 3166-2 na Slovenijo"@sl ;
                                   rdfs:label "Liste der ISO-3166-2-Codes für Slowenien"@de ;
                                   skos:prefLabel "Liste der ISO-3166-2-Codes für Slowenien"@de ;
                                   dct:publisher ?ISOPublisher .

  emergelModules:SIMunicipalitiesFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the municipalities of Slovenia" ;
                                   skos:prefLabel "FIPS codes for the municipalities of Slovenia" ;
                                   rdfs:label "FIPS codes for the municipalities of Slovenia"@en;
                                   skos:prefLabel "FIPS codes for the municipalities of Slovenia"@en ;
                                   rdfs:label "Codici FIPS per la Slovenia"@it ;
                                   skos:prefLabel "Codici FIPS per la Slovenia"@it ;
                                   rdfs:label "Kode FIPS na Slovenijo"@sl ;
                                   skos:prefLabel "Kode FIPS na Slovenijo"@sl ;
                                   rdfs:label "Liste der FIPS-Codes für Slowenien" @de ;
                                   skos:prefLabel "Liste der FIPS-Codes für Slowenien"@de ;
                                   dct:publisher ?FIPSPublisher .

  emergelModules:SIMunicipalitiesSURSCodeList a emergelModules:Country;
                                   rdfs:label "SURS (Statistični urad Republike Slovenije) codes for the municipalities of Slovenia" ;
                                   skos:prefLabel "SURS (Statistični urad Republike Slovenije) codes for the municipalities of Slovenia" ;
                                   rdfs:label "SURS (Statistični urad Republike Slovenije) codes for the municipalities of Slovenia"@en;
                                   skos:prefLabel "SURS (Statistični urad Republike Slovenije) codes for the municipalities of Slovenia"@en ;
                                   rdfs:label "Codici SURS (Statistični urad Republike Slovenije) per la Slovenia"@it ;
                                   skos:prefLabel "Codici SURS (Statistični urad Republike Slovenije) per la Slovenia"@it ;
                                   rdfs:label "Kode SURS (Statistični urad Republike Slovenije) na Slovenijo"@sl ;
                                   skos:prefLabel "Kode SURS (Statistični urad Republike Slovenije) na Slovenijo"@sl ;
                                   rdfs:label "Liste der SURS-Codes für Slowenien (Statistični urad Republike Slovenije)" @de ;
                                   skos:prefLabel "Liste der SURS-Codes für Slowenien (Statistični urad Republike Slovenije)"@de ;
                                   dct:publisher ?SURSPublisher .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?slLabel ;
                  skos:altLabel ?slaltLabel ;
                  skos:prefLabel ?bsLabel ;
                  skos:prefLabel ?hrLabel ;
                  skos:prefLabel ?itLabel ;
                  skos:prefLabel ?deLabel ;
                  skos:prefLabel ?mkLabel ;
                  skos:prefLabel ?shLabel ;
                  skos:prefLabel ?srLabel ;
                  skos:prefLabel ?enLabel ;
                  skos:prefLabel ?huLabel ;
                  skos:inScheme emergelModules:SIMunicipalitiesList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:broader ?regionSupResource .
  
  ?regionSupResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:SIMunicipalitiesISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:SIMunicipalitiesFIPSCodeList;
                  emergel:codeOf ?regionResource .

  ?regionSURSCodeResource a skos:Concept;
                  rdfs:label ?SURSCode ;
                  skos:prefLabel ?SURSCode ; 
                  skos:inScheme emergelModules:SIMunicipalitiesSURSCodeList;
                  emergel:codeOf ?regionResource .
}
