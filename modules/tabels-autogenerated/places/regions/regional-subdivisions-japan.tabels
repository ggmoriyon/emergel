PREFIX project: <http://idi.fundacionctic.org/tabels/project/KPProvinces/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/SRregions/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode, ?jpalt, ?region, ?zh, ?ISOCode, ?ko, ?ru, ?runote, ?ennote, ?konote, ?FIPSCode, ?en] IN horizontal 
    
  LET ?regionResource = resource(concat("JP_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("JP_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("JP_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?jaLangLabel = setLangTag(?region, "ja")
  LET ?ja1LangLabel = setLangTag(?jpalt, "ja-Latn")
  LET ?ruLangLabel = setLangTag(?ru, "ru")
  LET ?koLangLabel = setLangTag(?ko, "ko")
  LET ?zhLangLabel = setLangTag(?zh, "zh")
  LET ?enLangLabel = setLangTag(?en, "en")
  LET ?enNoteLabel = setLangTag(?ennote, "en")
  LET ?ruNoteLabel = setLangTag(?runote, "ru")
  LET ?koNoteLabel = setLangTag(?konote, "ko")
  
    

CONSTRUCT {
  
  emergelModules:JPPrefectureList a emergelModules:Country;
                              rdfs:label "Prefectures of Japan" ;
                              skos:prefLabel "Prefectures of Japan" ;
                              rdfs:label "Prefectures of Japan"@en ;
                              skos:prefLabel "Prefectures of Japan"@en ;
                              rdfs:label "Префектуры Японии"@ru ;
                              skos:prefLabel "Префектуры Японии"@ru ;
                              rdfs:label "日本: 道府県"@ja ;
                              skos:prefLabel "日本: 道府県"@ja ;
                              rdfs:label "일본의 도도부현"@ko ;
                              skos:prefLabel "일본의 도도부현"@ko ;
                              rdfs:label "日本都道府縣"@zh ;
                              skos:prefLabel "日本都道府縣"@zh .
  
  emergelModules:JPPrefectureISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the prefectures of Japan" ;
                                   skos:prefLabel "ISO codes for the prefectures of Japan" ;
                                   rdfs:label "ISO codes for the prefectures of Japan"@en ;
                                   skos:prefLabel "ISO codes for the prefectures of Japan"@en ;
                                   rdfs:label "日本: 道府県 - ISO 3166-2:JP"@ja ;
                                   skos:prefLabel "日本: 道府県 - ISO 3166-2:JP"@ja .

  emergelModules:JPPrefectureFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the prefectures of Japan" ;
                                   skos:prefLabel "FIPS codes for the prefectures of Japan" ;
                                   rdfs:label "FIPS codes for the prefectures of Japan"@en ;
                                   skos:prefLabel "FIPS codes for the prefectures of Japan"@en ;
                                   rdfs:label "日本: 道府県 - FIPS"@ja ;
                                   skos:prefLabel "日本: 道府県 - FIPS"@ja .
  
 
}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?jaLangLabel ;
                  skos:prefLabel ?ja1LangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?koLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?ruLangLabel ;
                  skos:prefLabel ?zhLangLabel ;
                  skos:note ?enNoteLabel ;
                  skos:note ?ruNoteLabel ;
                  skos:note ?koNoteLabel ;
                  skos:inScheme emergelModules:JPPrefectureList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:broader ?ISOResource .

  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:JPPrefectureISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:JPPrefectureFIPSCodeList;
                  emergel:codeOf ?regionResource .
}
