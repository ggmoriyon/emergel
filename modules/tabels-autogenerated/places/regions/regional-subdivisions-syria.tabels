PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsSY/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsSY/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?araltlabel,?araltlabel1,?arlabel,?kmrlabel,?kmrlatlabel,?ckblatlabel,?ckblabel,?enlabel,?FIPSCode,?ISOCode,?trlabel,?traltlabel,?helabel,?hylabel] IN horizontal 
    
  LET ?regionResource = resource(concat("SY_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("SY_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("SY_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "ar-Latn")
  LET ?araltLanglabel = setLangTag(?araltlabel, "ar-Latn")
  LET ?araltLanglabel1 = setLangTag(?araltlabel1, "ar-Latn")
  LET ?arLanglabel = setLangTag(?arlabel, "ar")
  LET ?enLanglabel = setLangTag(?enlabel, "en")
  LET ?trLanglabel = setLangTag(?trlabel, "tr")
  LET ?heLanglabel = setLangTag(?helabel, "he")
  LET ?hyLanglabel = setLangTag(?hylabel, "hy")
  LET ?ckbLanglabel = setLangTag(?ckblabel, "ckb")
  LET ?ckblatLanglabel = setLangTag(?ckblatlabel, "ckb")

  {
WHEN not matches(?kmrlabel,"") DO
LET ?kmrLangLabel = setLangTag(?kmrlabel, "kmr")
    ;
WHEN not matches(?kmrlatlabel,"") DO
LET ?kmrlatLangLabel = setLangTag(?kmrlatlabel, "kmr-Latn")
    ;
WHEN not matches(?trlabel,"") DO
LET ?traltLangLabel = setLangTag(?traltlabel, "tr")
  }

CONSTRUCT {
  
  emergelModules:SYGovernorateList a emergelModules:Country;
                              rdfs:label "Governorates of Syria" ;
                              skos:prefLabel "Governorates of Syria" ;
                              rdfs:label "Governorates of Syria"@en ;
                              skos:prefLabel "Governorates of Syria"@en ;
                              rdfs:label "Gouvernorats de Syria"@fr ;
                              skos:prefLabel "Gouvernorats de Syria"@fr ;
                              rdfs:label "محافظات سوريا"@ar ;
                              skos:prefLabel "محافظات سوريا"@ar ;
                              rdfs:label "Suriye'nin illeri"@tr ;
                              skos:prefLabel "Suriye'nin illeri"@tr ;
                              rdfs:label "پارێزگاکانی سووریا"@ckb ;
                              skos:prefLabel "پارێزگاکانی سووریا"@ckb ;
                              rdfs:label "מחוזות סוריה"@he ;
                              skos:prefLabel "מחוזות סוריה"@he ;
                              rdfs:label "Սիրիայի մարզեր"@hy ;
                              skos:prefLabel "Սիրիայի մարզեր"@hy ;
                              rdfs:label "mhafzat swrya"@ar-Latn ;
                              skos:prefLabel "mhafzat swrya"@ar-Latn .
  
  emergelModules:SYGovernorateISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the governorates of Syria" ;
                                   skos:prefLabel "ISO codes for the governorates of Syria" ;
                                   rdfs:label "ISO codes for the governorates of Syria"@en ;
                                   skos:prefLabel "ISO codes for the governorates of Syria"@en ;
                                   rdfs:label "Codes ISO 3166-2:SY pour les gouvernorats de Syria"@fr ;
                                   skos:prefLabel "Codes ISO 3166-2:SY pour les gouvernorats de Syria"@fr ;
                                   rdfs:label "ISO 3166-2:SY - محافظات سوريا"@ar ;
                                   skos:prefLabel "ISO 3166-2:SY - محافظات سوريا"@ar ;
                                   rdfs:label "ISO 3166-2:SY - پارێزگاکانی سووریا"@ckb ;
                                   skos:prefLabel "ISO 3166-2:SY - پارێزگاکانی سووریا"@ckb ;
                                   rdfs:label "ISO 3166-2:SY - מחוזות סוריה"@he ;
                                   skos:prefLabel "ISO 3166-2:SY - מחוזות סוריה"@he ;
                                   rdfs:label "ISO 3166-2:SY - mhafzat swrya"@ar-Latn ;
                                   skos:prefLabel "ISO 3166-2:SY - mhafzat swrya"@ar-Latn ;
                                   rdfs:label "ISO 3166-2:SY - Սիրիայի մարզեր"@hy ;
                                   skos:prefLabel "ISO 3166-2:SY - Սիրիայի մարզերה"@hy .

  emergelModules:SYGovernorateFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the governorates of Syria" ;
                                   skos:prefLabel "FIPS codes for the governorates of Syria" ;
                                   rdfs:label "FIPS codes for the governorates of Syria"@en ;
                                   skos:prefLabel "FIPS codes for the governorates of Syria"@en ;
                                   rdfs:label "Codes FIPS pour les gouvernorats de Syria"@fr ;
                                   skos:prefLabel "Codes FIPS pour les gouvernorats de Syria"@fr ;
                                   rdfs:label "FIPS - محافظات سوريا"@ar ;
                                   skos:prefLabel "FIPS - محافظات سوريا"@ar ;
                                   rdfs:label "FIPS - پارێزگاکانی سووریا"@ckb ;
                                   skos:prefLabel "FIPS - پارێزگاکانی سووریا"@ckb ;
                                   rdfs:label "FIPS - מחוזות סוריה"@he ;
                                   skos:prefLabel "FIPS - מחוזות סוריה"@he ;
                                   rdfs:label "FIPS - wlayat twns"@ar-Latn ;
                                   skos:prefLabel "FIPS - wlayat twns"@ar-Latn ;
                                   rdfs:label "FIPS - Սիրիայի մարզեր"@hy ;
                                   skos:prefLabel "FIPS - Սիրիայի մարզեր"@hy .

}


CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?arLanglabel ;
                  skos:prefLabel ?enLanglabel ;
                  skos:prefLabel ?trLanglabel ;
                  skos:prefLabel ?heLanglabel ;
                  skos:prefLabel ?hyLanglabel ;
                  skos:prefLabel ?ckbLanglabel ;
                  skos:prefLabel ?ckblatLanglabel ;
                  skos:altLabel ?araltLanglabel ;
                  skos:altLabel ?araltLanglabel1 ;
                  skos:inScheme emergelModules:SYGovernorateList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:SYGovernorateISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:SYGovernorateFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?kmrLangLabel .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?kmrlatLangLabel .
}

CONSTRUCT {
  ?regionResource skos:altLabel ?traltLangLabel .
}