PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsLB/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsLB/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?altlabel1,?altlabel2,?ISOCode,?FIPSCode,?enlabel,?helabel,?hylabel,?hbolabel] IN horizontal 
    
  LET ?regionResource = resource(concat("LB_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("LB_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("LB_DIVISION_LEVEL_1_FIPS_CODE_",replace(?FIPSCode,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  LET ?region = setLangTag(?region, "ar")
  LET ?altLanglabel1 = setLangTag(?altlabel1, "ar-Latn")
  LET ?altLanglabel2 = setLangTag(?altlabel2, "ar-Latn")
  LET ?enLanglabel = setLangTag(?enlabel, "en")
  LET ?hyLanglabel = setLangTag(?hylabel, "hy")
  LET ?heLanglabel = setLangTag(?helabel, "he")


  WHEN not matches(?hbolabel,"") DO
  LET ?hboLangLabel = setLangTag(?hbolabel, "hbo")

CONSTRUCT {
  
  emergelModules:LBGovernorateList a emergelModules:Country;
                              rdfs:label "Governorates of Lebanon" ;
                              skos:prefLabel "Governorates of Lebanon" ;
                              rdfs:label "Governorates of Lebanon"@en ;
                              skos:prefLabel "Governorates of Lebanon"@en ;
                              rdfs:label "מחוזות לבנון"@he ;
                              skos:prefLabel "מחוזות לבנון"@he ;
                              rdfs:label "محافظات لبنان"@ar ;
                              skos:prefLabel "محافظات لبنان"@ar ;
                              rdfs:label "Լիբանանի վարչական բաժանում"@hy ;
                              skos:prefLabel "Լիբանանի վարչական բաժանում"@hy .
  
  emergelModules:LBGovernorateISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the governorates of Lebanon" ;
                                   skos:prefLabel "ISO codes for the governorates of Lebanon" ;
                                   rdfs:label "ISO codes for the governorates of Lebanon"@en ;
                                   skos:prefLabel "ISO codes for the governorates of Lebanon"@en ;
                                   rdfs:label "ISO 3166-2:LB - מחוזות לבנון"@he ;
                                   skos:prefLabel "ISO 3166-2:LB - מחוזות לבנון"@he ;
                                   rdfs:label "ISO 3166-2:LB - محافظات لبنان"@ar ;
                                   skos:prefLabel "ISO 3166-2:LB - محافظات لبنان"@ar ;
                                   rdfs:label "ISO 3166-2:LB - Լիբանանի վարչական բաժանում"@hy ;
                                   skos:prefLabel "ISO 3166-2:LB - Լիբանանի վարչական բաժանում"@hy .

  emergelModules:LBGovernorateFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the governorates of Lebanon" ;
                                   skos:prefLabel "FIPS codes for the governorates of Lebanon" ;
                                   rdfs:label "FIPS codes for the governorates of Lebanon"@en ;
                                   skos:prefLabel "FIPS codes for the governorates of Lebanon"@en ;
                                   rdfs:label "FIPS - מחוזות לבנון"@he ;
                                   skos:prefLabel "FIPS - מחוזות לבנון"@he ;
                                   rdfs:label "FIPS - مناطق إسرائيل"@ar ;
                                   skos:prefLabel "FIPS - مناطق إسرائيل"@ar ;
                                   rdfs:label "FIPS - Լիբանանի վարչական բաժանում"@hy ;
                                   skos:prefLabel "FIPS - Լիբանանի վարչական բաժանում"@hy .

}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ;
                  skos:prefLabel ?enLanglabel ;
                  skos:prefLabel ?hyLanglabel ;
                  skos:prefLabel ?heLanglabel ;
                  skos:altLabel ?altLanglabel2 ; 
                  skos:altLabel ?altLanglabel1 ; 
                  skos:inScheme emergelModules:LBGovernorateList;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource ;
                  skos:broader ?ISOResource .
  
  ?ISOResource skos:narrower ?regionResource .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:LBGovernorateISOCodeList;
                  emergel:codeOf ?regionResource .

  ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?FIPSCode ;
                  skos:prefLabel ?FIPSCode ; 
                  skos:inScheme emergelModules:LBGovernorateFIPSCodeList;
                  emergel:codeOf ?regionResource .
}

CONSTRUCT {
  ?regionResource skos:prefLabel ?hboLangLabel  .
}