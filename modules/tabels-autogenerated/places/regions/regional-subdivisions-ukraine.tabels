PREFIX project: <http://idi.fundacionctic.org/tabels/project/regionsUA/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/regionsUA/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?regionCode,?region,?altlabel,?rulabel,?enlabel,?note_uk,?note_ru,?note_en,?hylabel,?kalabel,?trlabel,?belabel,?bglabel,?crhlabel,?ellabel,?hulabel,?pllabel,?rolabel,?sklabel,?ruelabel,?ISOCode,?koatuu,?fips,?WesUk,?EasUk,?SouUk,?CenUk] IN horizontal 
    
  LET ?regionResource = resource(concat("UA_DIVISION_LEVEL_1_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionCodeResource = resource(concat("UA_DIVISION_LEVEL_1_ISO_CODE_",replace(?regionCode,"-","_")),emergelModules)
  LET ?regionFIPSCodeResource = resource(concat("UA_DIVISION_LEVEL_1_FIPS_CODE_",replace(?fips,"-","_")),emergelModules)
  LET ?regionKOATUUCodeResource = resource(concat("UA_DIVISION_LEVEL_1_KOATUU_CODE_",replace(?koatuu,"-","_")),emergelModules)
  LET ?ISOResource = resource(?ISOCode,euCountry)
  /*name of the country in local languages*/
  LET ?rueCountryLabel = setLangTag("Україна", "rue")
  LET ?crhCountryLabel = setLangTag("Ukraina", "crh")
  LET ?yiCountryLabel = setLangTag("אוקראינע", "yi")
  LET ?kaCountryLabel = setLangTag("უკრაინა", "ka")
  LET ?hyCountryLabel = setLangTag("Ուկրաինա", "hy")
  

  LET ?regionLangLabel = setLangTag(?region, "uk")
  LET ?ukAltLangLabel = setLangTag(?altlabel, "uk-Latn")
  LET ?ruLangLabel = setLangTag(?rulabel, "ru")
  LET ?enLangLabel = setLangTag(?enlabel, "en")
  LET ?bgLangLabel = setLangTag(?bglabel, "bg")
  LET ?beLangLabel = setLangTag(?belabel, "be")
  LET ?trLangLabel = setLangTag(?trlabel, "tr")
  LET ?kaLangLabel = setLangTag(?kalabel, "ka")
  LET ?hyLangLabel = setLangTag(?hylabel, "hy")
  LET ?roLangLabel = setLangTag(?rolabel, "ro")
  LET ?plLangLabel = setLangTag(?pllabel, "pl")
  LET ?skLangLabel = setLangTag(?sklabel, "sk")
  LET ?elLangLabel = setLangTag(?ellabel, "el")
  LET ?huLangLabel = setLangTag(?hulabel, "hu")
  LET ?rueLangLabel = setLangTag(?ruelabel, "rue")
  LET ?crhLangLabel = setLangTag(?crhlabel, "crh")
  LET ?note_ukLang = setLangTag(?note_uk, "uk")
  LET ?note_ruLang = setLangTag(?note_ru, "ru")
  LET ?note_enLang = setLangTag(?note_en, "en")
{
 WHEN matches(?WesUk,"yes") DO
  LET ?regionWesUkResource = resource(concat("UA_REGION_",replace(?regionCode,"-","_")),emergelModules);
 WHEN matches(?EasUk,"yes") DO
  LET ?regionEasUkResource = resource(concat("UA_REGION_",replace(?regionCode,"-","_")),emergelModules);
 WHEN matches(?SouUk,"yes") DO
  LET ?regionSouUkResource = resource(concat("UA_REGION_",replace(?regionCode,"-","_")),emergelModules);
 WHEN matches(?CenUk,"yes") DO
  LET ?regionCenUkResource = resource(concat("UA_REGION_",replace(?regionCode,"-","_")),emergelModules)
}

CONSTRUCT {
  
  emergelModules:UARegionList a emergelModules:Country;
                              rdfs:label "Regions of Ukraine" ;
                              skos:prefLabel "Regions of Ukraine" ;
                              rdfs:label "Regions of Ukraine"@en ;
                              skos:prefLabel "Regions of Ukraine"@en ;
                              rdfs:label "Адміністративний устрій України"@uk ;
                              skos:prefLabel "Адміністративний устрій України"@uk ;
                              rdfs:label "Административное деление Украины"@ru ;
                              skos:prefLabel "Административное деление Украины"@ru ;
                              rdfs:label "Administratyvnyj ustrij Ukrajiny"@uk-Latn ;
                              skos:prefLabel "Administratyvnyj ustrij Ukrajiny"@uk-Latn ;
                              rdfs:label "Împărțirea administrativă a Ucrainei"@ro ;
                              skos:prefLabel "Împărțirea administrativă a Ucrainei"@ro ;
                              rdfs:label "Περιφέρειες της Ουκρανίας"@el ;
                              skos:prefLabel "εριφέρειες της Ουκρανίας"@el ;
                              rdfs:label "Административно деление на Украйна"@bg ;
                              skos:prefLabel "Административно деление на Украйна"@bg ;
                              rdfs:label "Memuriy bölüvi"@crh ;
                              skos:prefLabel "Memuriy bölüvi"@crh ;
                              rdfs:label "Podział administracyjny Ukrainy"@pl ;
                              skos:prefLabel "Podział administracyjny Ukrainy"@pl ;
                              rdfs:label "Ukrajna közigazgatása"@hu ;
                              skos:prefLabel "Ukrajna közigazgatása"@hu ;
                              rdfs:label "Адміністрацыйны падзел Украіны: Спіс тэрытарыяльных утварэнняў Украіны"@be ;
                              skos:prefLabel "Адміністрацыйны падзел Украіны: Спіс тэрытарыяльных утварэнняў Украіны"@be ;
                              rdfs:label "Ukrayna'nın idari birimleri: Ukrayna'nın oblastları"@tr ;
                              skos:prefLabel "Ukrayna'nın idari birimleri: Ukrayna'nın oblastları"@tr ;
                              rdfs:label "Administratívne členenie Ukrajiny: oblastí a mestá"@sk ;
                              skos:prefLabel "Administratívne členenie Ukrajiny: oblastí a mestá"@sk ;
                              rdfs:label "უკრაინის ოლქების"@ka ;
                              skos:prefLabel "უკრაინის ოლქების"@ka ;
                              rdfs:label "Ուկրաինայի վարչական բաժանում"@hy ;
                              skos:prefLabel "Ուկրաինայի վարչական բաժանում"@hy .
  
  emergelModules:UARegionISOCodeList a emergelModules:Country;
                                   rdfs:label "ISO codes for the administrative subdivisions of Ukraine" ;
                                   skos:prefLabel "ISO codes for the administrative subdivisions of Ukraine" ;
                                   rdfs:label "ISO codes for the administrative subdivisions of Ukraine"@en ;
                                   skos:prefLabel "ISO codes for the administrative subdivisions of Ukraine"@en ;
                                   rdfs:label "Адміністративний устрій України - ISO 3166-2:UA"@uk ;
                                   skos:prefLabel "Адміністративний устрій України - ISO 3166-2:UA"@uk .

  emergelModules:UARegionFIPSCodeList a emergelModules:Country;
                                   rdfs:label "FIPS codes for the administrative subdivisions of Ukraine" ;
                                   skos:prefLabel "FIPS codes for the administrative subdivisions of Ukraine" ;
                                   rdfs:label "FIPS codes for the administrative subdivisions of Ukraine"@en ;
                                   skos:prefLabel "FIPS codes for the administrative subdivisions of Ukraine"@en ;
                                   rdfs:label "FIPS - Адміністративний устрій України"@uk ;
                                   skos:prefLabel "FIPS - Адміністративний устрій України"@uk .

  emergelModules:UARegionKOATUUCodeList a emergelModules:Country;
                                   rdfs:label "KOATUU codes for the regions of Ukraine"@en ;
                                   skos:prefLabel "KOATUU codes for the regions of Ukraine"@en ;
                                   rdfs:label "KOATUU codes for the regions of Ukraine" ;
                                   skos:prefLabel "KOATUU codes for the regions of Ukraine" ;
                                   rdfs:label "Класифікатор об'єктів адміністративно-територіального устрою України (КОАТУУ)"@uk ;
                                   skos:prefLabel "Класифікатор об'єктів адміністративно-територіального устрою України (КОАТУУ)"@uk .
  
  emergelModules:UAWesternRegionList a emergelModules:Country;
                              rdfs:label "Western regions of Ukraine"@en ;
                              skos:prefLabel "Western regions of Ukraine"@en ;
                              rdfs:label "Західна Україна"@uk ;
                              skos:prefLabel "Західна Україна"@uk .
  
  emergelModules:UAEasternRegionList a emergelModules:Country;
                              rdfs:label "Eastern regions of Ukraine"@en ;
                              skos:prefLabel "Eastern regions of Ukraine"@en ;
                              rdfs:label " Східна Україна"@uk ;
                              skos:prefLabel " Східна Україна"@uk .
  
  emergelModules:UASouthernRegionList a emergelModules:Country;
                              rdfs:label "Southern regions of Ukraine"@en ;
                              skos:prefLabel "Southern regions of Ukraine"@en ;
                              rdfs:label "Південна Україна"@uk ;
                              skos:prefLabel "Південна Україна"@uk .
  
  emergelModules:UACentralRegionList a emergelModules:Country;
                              rdfs:label "Central regions of Ukraine"@en ;
                              skos:prefLabel "Central regions of Ukraine"@en ;
                              rdfs:label "Центральна Україна"@uk ;
                              skos:prefLabel "Центральна Україна"@uk .
}
CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?regionLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:prefLabel ?ruLangLabel ;
                  skos:prefLabel ?roLangLabel ;
                  skos:prefLabel ?skLangLabel ;
                  skos:prefLabel ?elLangLabel ;
                  skos:prefLabel ?plLangLabel ;
                  skos:prefLabel ?huLangLabel ;
                  skos:prefLabel ?rueLangLabel ;
                  skos:prefLabel ?beLangLabel ;
                  skos:prefLabel ?kaLangLabel ;
                  skos:prefLabel ?hyLangLabel ;
                  skos:prefLabel ?trLangLabel ;
                  skos:prefLabel ?crhLangLabel ;
                  skos:altLabel ?ukAltLangLabel ;
                  skos:prefLabel ?bgLangLabel ;
                  skos:note ?note_ukLang ;
                  skos:note ?note_ruLang ;
                  skos:note ?note_enLang ;
                  skos:inScheme emergelModules:UARegionList;
                  skos:broader ?ISOResource ;
                  emergel:hasCode ?regionCodeResource ;
                  emergel:hasCode ?regionKOATUUCodeResource ;
                  emergel:hasCode ?regionFIPSCodeResource .
  
  ?ISOResource skos:narrower ?regionResource ;
              skos:prefLabel ?crhCountryLabel ;
              skos:prefLabel ?yiCountryLabel ;
              skos:prefLabel ?kaCountryLabel ;
              skos:prefLabel ?hyCountryLabel ;
              skos:prefLabel ?rueCountryLabel .
  
  ?regionCodeResource a skos:Concept;
                  rdfs:label ?regionCode ;
                  skos:prefLabel ?regionCode ; 
                  skos:inScheme emergelModules:UARegionISOCodeList;
                  emergel:codeOf ?regionResource .

   ?regionKOATUUCodeResource a skos:Concept;
                  rdfs:label ?koatuu ;
                  skos:prefLabel ?koatuu ; 
                  skos:inScheme emergelModules:UARegionKOATUUCodeList;
                  emergel:codeOf ?regionResource .
                  
   ?regionFIPSCodeResource a skos:Concept;
                  rdfs:label ?fips ;
                  skos:prefLabel ?fips ; 
                  skos:inScheme emergelModules:UARegionFIPSCodeList;
                  emergel:codeOf ?regionResource .                                  
}

CONSTRUCT {
  
  ?regionWesUkResource skos:inScheme emergelModules:UAWesternRegionList .
}

CONSTRUCT {
  
  ?regionEasUkResource skos:inScheme emergelModules:UAEasternRegionList .
}

CONSTRUCT {
  
  ?regionSouUkResource skos:inScheme emergelModules:UASouthernRegionList .
}

CONSTRUCT {
  
  ?regionCenUkResource skos:inScheme emergelModules:UACentralRegionList .
}