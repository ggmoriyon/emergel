PREFIX project: <http://idi.fundacionctic.org/tabels/project/corop-regionenNL/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/corop-regionenNL/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX nuts: <http://nuts.geovocab.org/id/>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>



FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?nuts3,?region,?enlabel,?nuts2,?nuts1,?ISOregionCode] IN horizontal 
    
  LET ?regionResource = resource(concat("NL_DIVISION_LEVEL_2_",replace(?nuts3,"-","_")),emergelModules)
  LET ?ISOResource = resource(concat("NL_DIVISION_LEVEL_1_",replace(?ISOregionCode,"-","_")),emergelModules)

  LET ?nlLangLabel = setLangTag(?region, "nl")
  LET ?enLangLabel = setLangTag(?enlabel, "en")

  LET ?nut1Res = resource(?nuts1, nuts)
  LET ?nut2Res = resource(?nuts2, nuts)
  LET ?nut3Res = resource(?nuts3, nuts)


    

CONSTRUCT {
  
  emergelModules:NLCOROPRegionenList a emergelModules:Country;
                              rdfs:label "COROP Regions of the Netherlands" ;
                              skos:prefLabel "COROP Regions of the Netherlands" ;
                              rdfs:label "COROP Regions of the Netherlands"@en ;
                              skos:prefLabel "COROP Regions of the Netherlands"@en ;
                              rdfs:label "COROP-Regionen"@nl ;
                              skos:prefLabel "COROP-Regionen"@nl .
  

  emergelModules:NUTS1CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 1 codes"@en .

  emergelModules:NUTS2CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 2 codes"@en .

  emergelModules:NUTS3CodeList a emergelModules:Country;
                                   rdfs:label "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en ;
                                   skos:prefLabel "Nomenclature of Territorial Units for Statistics (NUTS) level 3 codes"@en .
}

CONSTRUCT {
  ?regionResource a skos:Concept;
                  rdfs:label ?region ;
                  skos:prefLabel ?region ; 
                  skos:prefLabel ?nlLangLabel ;
                  skos:prefLabel ?enLangLabel ;
                  skos:inScheme emergelModules:NLCOROPRegionenList;
                  emergel:hasCode ?nut3Res ;
                  skos:broader ?ISOResource .

  ?ISOResource skos:narrower ?regionResource .


  ?nut3Res a skos:Concept;
           skos:inScheme emergelModules:NUTS3CodeList ;
           skos:broader ?nut2Res .

  ?nut2Res a skos:Concept;
           skos:inScheme emergelModules:NUTS2CodeList ;
           skos:narrower ?nut3Res .

  }
