PREFIX project: <http://idi.fundacionctic.org/tabels/project/nuclear/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/nuclear/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>


FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?station,?country,?ISOCode,?geodivision1,?geodivision2,?status,?reactorUnits,?capacity,?location,?bg,?be,?ca,?cs,?de,?es,?eu,?fi,?fr,?gu,?hi,?hu,?ja,?ko,?lt,?nl,?pt,?ro,?ru,?sk,?sv,?ta,?uk,?zh,?rualt] IN horizontal 
  
	LET	?stationResource = resource (concat("POWER_STATION_",replace(?station,"[^a-zA-Z0-9]","")),emergelModules)
 	LET ?ISOResource = resource(?ISOCode,euCountry)

{
WHEN not matches(?reactorUnits, "") DO
LET ?units = ?reactorUnits
 ;
WHEN not matches(?geodivision1, "") DO
LET ?geodivisionResource1 = resource(concat(substring(?geodivision1,0,2),"_DIVISION_LEVEL_1_",replace(?geodivision1,"-","_")),emergelModules)
 ;
WHEN not matches(?geodivision2, "") DO
LET ?geodivisionResource2 = resource(concat(substring(?geodivision2,0,2),"_DIVISION_LEVEL_2_",replace(?geodivision2,"-","_")),emergelModules)
 ;
WHEN not matches (?bg, "") DO
  LET ?bgLangLabel = setLangTag(?bg, "bg")
 ;
WHEN not matches (?be, "") DO
  LET ?beLangLabel = setLangTag(?be, "be")
;
WHEN not matches (?ca, "") DO
  LET ?caLangLabel = setLangTag(?ca, "ca")
;
WHEN not matches (?cs, "") DO
  LET ?csLangLabel = setLangTag(?cs, "cs")
  ;
WHEN not matches (?de, "") DO
  LET ?deLangLabel = setLangTag(?de, "de")
  ;
WHEN not matches (?cs, "") DO
  LET ?csLangLabel = setLangTag(?cs, "cs")
  ;
WHEN not matches (?es, "") DO
  LET ?esLangLabel = setLangTag(?es, "es")
  ;
WHEN not matches (?eu, "") DO
  LET ?euLangLabel = setLangTag(?eu, "eu")
  ;
WHEN not matches (?fi, "") DO
  LET ?fiLangLabel = setLangTag(?fi, "fi")
  ;
WHEN not matches (?fr, "") DO
  LET ?frLangLabel = setLangTag(?fr, "fr")
  ;
WHEN not matches (?gu, "") DO
  LET ?guLangLabel = setLangTag(?gu, "gu")
  ;
WHEN not matches (?hi, "") DO
  LET ?hiLangLabel = setLangTag(?hi, "hi")
  ;
WHEN not matches (?hu, "") DO
  LET ?huLangLabel = setLangTag(?hu, "hu")
  ;
WHEN not matches (?ja, "") DO
  LET ?jaLangLabel = setLangTag(?ja, "ja")
  ;
WHEN not matches (?ko, "") DO
  LET ?koLangLabel = setLangTag(?ko, "ko")
  ;
WHEN not matches (?lt, "") DO
  LET ?ltLangLabel = setLangTag(?lt, "lt")
;
WHEN not matches (?nl, "") DO
  LET ?nlLangLabel = setLangTag(?nl, "nl")
  ;
WHEN not matches (?pt, "") DO
  LET ?ptLangLabel = setLangTag(?pt, "pt")
  ;
WHEN not matches (?ro, "") DO
  LET ?roLangLabel = setLangTag(?ro, "ro")
  ;
WHEN not matches (?ru, "") DO
  LET ?ruLangLabel = setLangTag(?ru, "ru")
  ;
WHEN not matches (?sk, "") DO
  LET ?skLangLabel = setLangTag(?sk, "sk")
  ;
WHEN not matches (?sv, "") DO
  LET ?svLangLabel = setLangTag(?sv, "sv")
    ;
WHEN not matches (?ta, "") DO
  LET ?taLangLabel = setLangTag(?ta, "ta")
  ;
WHEN not matches (?uk, "") DO
  LET ?ukLangLabel = setLangTag(?uk, "uk")
  ;
WHEN not matches (?zh, "") DO
  LET ?zhLangLabel = setLangTag(?zh, "zh")
  ;
WHEN not matches (?rualt, "") DO
  LET ?rualtLangLabel = setLangTag(?rualt, "ru")
}

CONSTRUCT {
  emergelModules:PowerStationList a emergelModules:Facility;
                              rdfs:label "Power stations" ;
                              skos:prefLabel "Power stations" ;
                              rdfs:label "Power stations"@en ;
                              skos:prefLabel "Power stations"@en .
  
  emergelModules:NuclearPowerStationList a emergelModules:Facility;
                              rdfs:label "Nuclear power stations" ;
                              skos:prefLabel "Nuclear power stations" ;
                              rdfs:label "Nuclear power stations"@en ;
                              skos:prefLabel "Nuclear power stations"@en ;
                              rdfs:label "Centrales nucleares"@es ;
                              skos:prefLabel "Centrales nucleares"@es ;
                              rdfs:label "Centrals nuclears"@ca ;
                              skos:prefLabel "Centrals nuclears"@ca ;
                              rdfs:label "Kernkraftwerke"@de ;
                              skos:prefLabel "Kernkraftwerke"@de ;
                              rdfs:label "Centrales nucléaires"@fr ;
                              skos:prefLabel "Centrales nucléaires"@fr ;
                              rdfs:label "Centrais nucleares"@pt ;
                              skos:prefLabel "Centrais nucleares"@pt ;
                              rdfs:label "Kerncentrales"@nl ;
                              skos:prefLabel "Kerncentrales"@nl ;
                              rdfs:label "核电站"@zh ;
                              skos:prefLabel "核电站"@zh ;
                              rdfs:label "Атомные электростанции"@ru ;
                              skos:prefLabel "Атомные электростанции"@ru ;
                              rdfs:label "Atomkraftværker"@da ;
                              skos:prefLabel "Atomkraftværker"@da ;
                              rdfs:label "Centrali elettronucleari"@it ;
                              skos:prefLabel "Centrali elettronucleari"@it .
  
  
}

CONSTRUCT {
  ?stationResource a skos:Concept ;
                   rdfs:label ?station;
                   skos:prefLabel ?station ;
                   skos:inScheme emergelModules:PowerStationList ;
                   skos:inScheme emergelModules:NuclearPowerStationList ;
                   emergel:inCountry ?ISOResource ;
                   emergel:status ?status ;
                   skos:note ?location ;
                   emergel:powerCapacityMW ?capacity .
}

CONSTRUCT {
  ?stationResource emergel:reactorUnits ?units .
}

CONSTRUCT {
  ?stationResource emergel:geoDivision ?geodivisionResource1 .
  ?geodivisionResource1 emergel:energyInfrastructure ?stationResource .
}

CONSTRUCT {
  ?stationResource emergel:geoDivision ?geodivisionResource2 .
  ?geodivisionResource2 emergel:energyInfrastructure ?stationResource .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?beLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?bgLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?caLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?csLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?deLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?esLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?euLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?fiLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?frLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?guLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?hiLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?huLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?jaLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?koLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?ltLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?nlLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?roLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?ruLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?skLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?svLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?taLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?ukLangLabel .
}

CONSTRUCT {
  ?stationResource skos:prefLabel ?zhLangLabel .
}

CONSTRUCT {
  ?stationResource skos:altLabel ?rualtLangLabel .
}