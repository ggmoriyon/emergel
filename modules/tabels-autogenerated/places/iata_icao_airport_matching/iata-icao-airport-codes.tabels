 
@JENARULE("[hasCode: (?x emergel:hasCode ?z) (?x emergel:hasCode ?y) (?z skos:inScheme emergelModules:ICAOAirportClassification) (?y skos:inScheme emergelModules:IATAAirportClassification) -> (?z skos:exactMatch ?y) (?y skos:exactMatch ?z)]")
PREFIX project: <http://idi.fundacionctic.org/tabels/project/IATA_ICAO_AIRPORT_MATCHING/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/IATA_ICAO_AIRPORT_MATCHING/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#>
PREFIX euCountry:<http://publications.europa.eu/resource/authority/country/>

FOR ?rowId IN rows FILTER get-row(?rowId)
    MATCH [?IATACode, ?ICAOCode, ?AirportName, ?Location, ?geodivision1, ?geodivision2, ?geodivision3] IN horizontal 
  LET ?AirportResource = resource(concat("AIRPORT_",replace(replace(?AirportName,"[^a-zA-Z0-9]",""),",","")), emergelModules)
LET ?IATAPublisher = resource("STANDARDIZATION_ORGANIZATION_IATA",emergelModules)
LET ?ICAOPublisher = resource("STANDARDIZATION_ORGANIZATION_ICAO",emergelModules)

  {
    WHEN contains(?Location,",") DO
      LET ?CountryResource = resource(replace(substring(?Location,last-index-of(?Location,",")),"[ ,]",""),euCountry)
  ;
    WHEN not matches(?ICAOCode,"") DO
    LET ?ICAOCodeResource = resource(concat("ICAO_AIRPORT_CODE_",replace(?ICAOCode," ","")),emergelModules)
        LET ?ICAOLabel = replace(?ICAOCode, "", "")
  ;
    WHEN not matches(?IATACode,"") DO
    LET ?IATACodeResource = resource(concat("IATA_AIRPORT_CODE_",replace(?IATACode," ","")),emergelModules)
        LET ?IATALabel = replace(?IATACode, "", "")
  ;
    WHEN not matches(?geodivision1, "") DO
    LET ?geodivisionResource1 = resource(concat(substring(?geodivision1,0,2),"_DIVISION_LEVEL_1_",replace(?geodivision1,"-","_")),emergelModules)
 ;
    WHEN not matches(?geodivision2, "") DO
    LET ?geodivisionResource2 = resource(concat(substring(?geodivision2,0,2),"_DIVISION_LEVEL_2_",replace(?geodivision2,"-","_")),emergelModules)
  }

CONSTRUCT {
  
    
    emergelModules:ICAOAirportClassification a emergelModules:Airport ;
                               rdfs:label "ICAO airport designators" ;
                               skos:prefLabel "ICAO airport designators" ;
                               dct:publisher ?ICAOPublisher .
  
    emergelModules:IATAAirportClassification a emergelModules:Airport ;
                               rdfs:label "IATA airport designators" ;
                               skos:prefLabel "IATA airport designators" ;
                               dct:publisher ?IATAPublisher .
    
    emergelModules:AirportClassification a emergelModules:Airport ;
                               rdfs:label "Airports" ;
                               skos:prefLabel "Airports" .
  
}
  
CONSTRUCT {
    ?ICAOCodeResource a skos:Concept ;
                      skos:inScheme emergelModules:ICAOAirportClassification ;
                      skos:notation ?ICAOLabel ;
                      rdfs:label ?ICAOLabel ;
                      skos:prefLabel ?ICAOLabel ;
                      emergel:codeOf ?AirportResource.
  
    ?AirportResource emergel:hasCode ?ICAOCodeResource .
                 
}

CONSTRUCT{
  ?AirportResource a skos:Concept ; 
                     skos:inScheme emergelModules:AirportClassification ;
                     rdfs:label ?AirportName ;
                     skos:prefLabel ?AirportName .
}

CONSTRUCT{
  ?AirportResource emergel:country ?CountryResource . 
}

CONSTRUCT{
    ?IATACodeResource a skos:Concept ;
                      skos:inScheme emergelModules:IATAAirportClassification ;
                      skos:notation ?IATALabel ;
                      rdfs:label ?IATALabel ;
                      skos:prefLabel ?IATALabel ;
                      emergel:codeOf ?AirportResource .
  
    ?AirportResource emergel:hasCode ?IATACodeResource .
}

CONSTRUCT {
  ?AirportResource emergel:geoDivision ?geodivisionResource1 .
  ?geodivisionResource1 emergel:transportationInfrastructure ?AirportResource .
}

CONSTRUCT {
  ?AirportResource emergel:geoDivision ?geodivisionResource2 .
  ?geodivisionResource2 emergel:transportationInfrastructure ?AirportResource .
}