PREFIX project: <http://idi.fundacionctic.org/tabels/project/ICAO_aircraft_helicopters/>
PREFIX my: <http://idi.fundacionctic.org/tabels/project/ICAO_aircraft_helicopters/resource/>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX dcat: <http://www.w3.org/ns/dcat#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX foaf: <http://xmlns.com/foaf/0.1/#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX emergelModules: <http://purl.org/emergel/modules#> 
PREFIX emergel: <http://purl.org/emergel/core#> 

FOR ?rowId IN rows FILTER get-row(?rowId)
  MATCH [?ICAOCode,?manufacturer,?rawModel,?wC,?altLabel1,?altLabel2,?altLabel3] IN horizontal 
    LET ?aircraftResource = resource(concat("AIRCRAFT_MODEL_",replace(?rawModel,"[ 'áéíóúýćàèìòùäëïöüâêîôûãõůžřčšěđñ\"-]","")), emergelModules)
    LET ?model = setLangTag(replace(?rawModel,"",""),"en")
    
    LET ?ICAOResource = resource(concat("ICAO_AIRCRAFT_CODE_",replace(?ICAOCode," ","")), emergelModules)
    LET ?aircraftCodeLabel = replace(?ICAOCode," ","")
   { 
     
    WHEN contains(?manufacturer,"/") DO 
       LET ?resourceManufacturer = resource(concat("AIRCRAFT_MANUFACTURER_",replace(substring-before(?manufacturer,"/"),"[^A-Za-z0-9]","_")),emergelModules)
       LET ?resourceManufacturer2 = resource(concat("AIRCRAFT_MANUFACTURER_",replace(substring-after(?manufacturer,"/"),"[^A-Za-z0-9]","_")),emergelModules) 
       LET ?labelManufacturer =  substring-before(?manufacturer,"/")
       LET ?labelLangManufacturer =  setLangTag(substring-before(?manufacturer,"/"), "en" )
       LET ?labelManufacturer2 =  substring-after(?manufacturer,"/") 
       LET ?labelLangManufacturer2 =  setLangTag(substring-after(?manufacturer,"/"), "en" )
       {WHEN not matches(?altLabel1,"") DO
          LET ?label1 = ?altLabel1
          WHEN not matches(?altLabel2,"") DO
           LET ?label2 = ?altLabel2
       ;
        WHEN not matches(?altLabel3,"") DO
          LET ?label3 = ?altLabel3                                
       }
    ;
    WHEN NOT contains(?manufacturer,"/") DO
        LET ?resourceManufacturer = resource(concat("AIRCRAFT_MANUFACTURER_",replace(?manufacturer,"[^A-Za-z0-9]","_")),emergelModules)
        LET ?labelManufacturer =  ?manufacturer
        LET ?labelLangManufacturer =  setLangTag(?manufacturer, "en" )
        LET ?ICAOPublisher = resource("STANDARDIZATION_ORGANIZATION_ICAO",emergelModules)
        WHEN not matches(?altLabel1,"") DO
          LET ?label1 = ?altLabel1
        WHEN not matches(?altLabel2,"") DO
          LET ?label2 = ?altLabel2
                                     
    ;
     WHEN not matches(?wC,"n/a") DO
      LET ?wakeCategory = ?wC
   
   }  

CONSTRUCT {
    emergelModules:ICAOAircraftClassification a emergelModules:Aircraft ;
                                       skos:prefLabel "ICAO aircraft designators" ;
                                       rdfs:label "ICAO aircraft designators" ;
                                       dct:publisher ?ICAOPublisher .

  
    emergelModules:AircraftClassification a emergelModules:Aircraft ;
                        skos:prefLabel "Aircrafts" ;
                        rdfs:label "Aircrafts" .
    
    emergelModules:ManufacturerClassification a emergelModules:Manufacturer ;
                             skos:prefLabel "Airplane & helicopter manufacturers" ;
                             rdfs:label "Airplane & helicopter manufacturers" .
}

CONSTRUCT {
    ?aircraftResource a skos:Concept ;
              emergel:manufacturedBy ?resourceManufacturer ;
                      skos:inScheme emergelModules:AircraftClassification ;
                      rdfs:label ?model ;
                      skos:prefLabel ?model  .
    
    ?resourceManufacturer a skos:Concept ;
                          rdfs:label ?labelManufacturer ;
                          skos:prefLabel ?labelManufacturer ;
                          skos:inScheme emergelModules:ManufacturerClassification ;
                          emergel:manufactures ?aircraftResource
}

CONSTRUCT {
    ?aircraftResource emergel:manufacturedBy ?resourceManufacturer2 .
    
    ?resourceManufacturer2 a skos:Concept ;
                          rdfs:label ?labelManufacturer2 ;
                          skos:prefLabel ?labelManufacturer2 ;
                          skos:inScheme emergelModules:ManufacturerClassification ;
                          emergel:manufactures ?aircraftResource .
}

CONSTRUCT{
    ?ICAOResource a skos:Concept ;
                  skos:inScheme emergelModules:ICAOAircraftClassification ;
                  skos:notation ?aircraftCodeLabel ;
                  rdfs:label ?aircraftCodeLabel ;
                  emergel:codeOf ?aircraftResource .
            
    ?aircraftResource emergel:hasCode ?ICAOResource .              
}

CONSTRUCT {
    
    ?aircraftResource emergel:wakeCategory ?wakeCategory .
}

CONSTRUCT {
    
    ?resourceManufacturer skos:altLabel ?label1 .
}

CONSTRUCT {
    
    ?resourceManufacturer skos:altLabel ?label2 .
}
CONSTRUCT {
    
    ?resourceManufacturer2 skos:altLabel ?label3 .
}