#!/bin/bash

# this script uses rdfcat
# see https://jena.apache.org/documentation/javadoc/jena/jena/rdfcat.html

source ./utils.sh
	
############## BEGIN RDF MERGE AND ZIP (RDFCAT) ###################### 

List_of_files_xml=""
List_of_files_ttl=""
File_path=""

#borrar el fichero de salida anterior
rm -f ${RDFCAT_OUTPUT_FILE}
rm -f ${RDFCAT_OUTPUT_FILE}.gz

# save and change IFS 
OLDIFS=$IFS
IFS=$'\n'
 
# read all file name into an array
fileArray_xml=($(find ../modules -type f  -name "*.rdf"))

# read all file name into an array
fileArray_ttl=($(find ../modules -type f  -name "*.ttl"))
 
# restore it 
IFS=$OLDIFS
 
# get length of an array
tLen_xml=${#fileArray_xml[@]}
tLen_ttl=${#fileArray_ttl[@]}

XML_SEPARATOR="-x"
# use for loop read all filenames
for (( i=0; i<${tLen_xml}; i++ ));
do
  File_path=$(abspath "${fileArray_xml[$i]}")
  List_of_files_xml+="${XML_SEPARATOR} ${File_path} "
done

TTL_SEPARATOR="-n"

# use for loop read all filenames
for (( i=0; i<${tLen_ttl}; i++ ));
do
  File_path=$(abspath "${fileArray_ttl[$i]}")
  List_of_files_ttl+="${TTL_SEPARATOR} ${File_path} "
done

#JENA_HOME=/Users/dayures/Documents/projects/jena/apache-jena/

export LOGGING="-Dlog4j.configuration=file:"$PWD"/rdfcat-log4j.properties"

$PWD/apache-jena-2.11.1/bin/rdfcat -out ${RDFCAT_RDF_OUTPUT_FORMAT} ${List_of_files_ttl} ${List_of_files_xml} -x issued/${OUTPUT_FILE_ISSUED} > ${RDFCAT_OUTPUT_FILE}
return_code=$?
if [ $return_code -ne 0 ]; then
  ERROR_FLAG=true
fi


# Check previous problems
if ($ERROR_FLAG); then
	exit_error #send non-zero value
fi

#gzip the output RDF file
gzip ${RDFCAT_OUTPUT_FILE}
return_code=$?
if [ $return_code -ne 0 ]; then
  ERROR_FLAG=true
fi

# Check previous problems
if ($ERROR_FLAG); then
	exit_error #send non-zero value
fi



############## END RDF MERGE AND ZIP (RDFCAT) ###################### 