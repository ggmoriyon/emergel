#!/bin/bash

source ./utils.sh

############## BEGIN UPLOAD EMERGEL ###################### 

# post file to server
# curl -i -v -X PUT -H "Content-Type: application/rdf+xml;charset=UTF-8"  -d @darius-hazards.rdf "http://localhost:8080/openrdf-sesame/repositories/emergel/statements?context=%3Chttp%3A%2F%2Fpurl.org%2Femergel%3E"

# properties
SERVER="http://192.168.4.170:8080"
# SERVER="http://192.168.99.100:49153"
# SERVER="http://localhost:8080"
REPOSITORY="emergel"
CONTEXT="%3Chttp%3A%2F%2Fpurl.org%2Femergel%3E"

# delete context <http://purl.org/emergel>
response=$(curl --write-out %{http_code} --silent --output sesame-delete.log -i -v -X DELETE "$SERVER/openrdf-sesame/repositories/$REPOSITORY/statements?context=$CONTEXT")

echo "response from curl 1 is $response"
if [ $response -ge 400 ]; then  
  echo ""
  echo "###############################################"
  echo "error on sending DELETE to Sesame. Log follows:"
  cat sesame-delete.log
  exit_error 
fi

# post file to server
response=$(curl --write-out %{http_code} --silent --output sesame-post-rdfcat.log  -i -v -X POST -H "Content-Type: text/turtle;charset=UTF-8"  --data-binary @$PWD/${RDFCAT_OUTPUT_FILE}.gz "$SERVER/openrdf-sesame/repositories/$REPOSITORY/statements?context=$CONTEXT")
echo "response from curl 2 is $response"
if [ $response -ge 400 ]; then
  echo ""
  echo "###############################################"	
  echo "error on sending POST with modules to Sesame. Log follows:"
  cat sesame-post-rdfcat.log
  echo ""
  echo "###############################################"
  echo "printing conflictive lines"
  printError sesame-post-rdfcat.log salida.ttl.gz
  exit_error
fi

response=$(curl --write-out %{http_code} --silent --output sesame-post-core.log  -i -v -X POST -H "Content-Type: application/rdf+xml;charset=UTF-8"  -d @$PWD/../core/emergel.owl "$SERVER/openrdf-sesame/repositories/$REPOSITORY/statements?context=$CONTEXT")
echo "response from curl 3 is $response"
if [ $response -ge 400 ]; then
  echo ""
  echo "###############################################"
  echo "error on sending POST with core to Sesame. Log follows:"
  cat sesame-post-core.log
  exit_error
fi


############## END UPLOAD EMERGEL ###################### 