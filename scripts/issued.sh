#!/bin/bash

source ./utils.sh

TEMPLATE_ISSUED="template-issued.tpl"

#borrar el fichero issued
rm -f issued/${OUTPUT_FILE_ISSUED}

#obtain the date in ISO 8601
date_issued=$(date +%FT%T%z)
sed "s/DATE_ISSUED/$date_issued/g;" issued/${TEMPLATE_ISSUED} > issued/${OUTPUT_FILE_ISSUED}